package com.blog.user.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.blog.common.JSONResponse;
import com.blog.post.model.Post;
import com.blog.post.service.IPostService;
import com.blog.user.model.User;
import com.blog.user.service.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/users")
@RestController
public class UserController {

  @Autowired
  private IUserService usersService;

  @Autowired
  private IPostService postsService;

  @GetMapping
  public JSONResponse<List<User>> findUsers() {
    JSONResponse<List<User>> response = new JSONResponse<List<User>>();
    response.setData(usersService.findAll());
    return response;
  }

  @GetMapping(value = "/{userId}")
  public JSONResponse<User> findUser(@PathVariable("userId") Long userId) {
    JSONResponse<User> response = new JSONResponse<User>();
    Optional<User> user = usersService.findOne(userId);

    if (user.isPresent()) {
      response.setData(user.get());

      return response;
    }

    HashMap<String, Object> errors = new HashMap<String, Object>();
    errors.put("code", 404);
    errors.put("message", "User Not Found");
    response.setErrors(errors);

    return response;
  }

  @PostMapping
  public JSONResponse<User> createUser(@RequestBody User newUser) {
    JSONResponse<User> response = new JSONResponse<User>();

    response.setData(usersService.create(newUser));

    return response;
  }

  @PutMapping(value = "/{userId}")
  public JSONResponse<User> updateUser(@PathVariable("userId") Long userId, @RequestBody User userData) {
    JSONResponse<User> response = new JSONResponse<User>();

    Optional<User> user = usersService.update(userId, userData);

    if (user.isEmpty()) {
      HashMap<String, Object> errors = new HashMap<String, Object>();
      errors.put("code", 404);
      errors.put("message", "User Not Found");
      response.setErrors(errors);
      return response;
    }

    response.setData(user.get());

    return response;
  }

  @DeleteMapping("{userId}")
  public JSONResponse<User> deleteUser(@PathVariable("userId") Long userId) {
    JSONResponse<User> response = new JSONResponse<User>();
    Optional<User> user = usersService.delete(userId);

    if (user.isPresent()) {
      response.setData(user.get());

      return response;
    }

    HashMap<String, Object> errors = new HashMap<String, Object>();
    errors.put("code", 404);
    errors.put("message", "User Not Found");
    response.setErrors(errors);

    return response;
  }

  @GetMapping("/{userId}/posts")
  public JSONResponse<List<Post>> getUsersPosts(@PathVariable Long userId) {
    JSONResponse<List<Post>> response = new JSONResponse<>();
    List<Post> usersPosts = postsService.findUsersPosts(userId);

    response.setData(usersPosts);

    return response;
  }

}