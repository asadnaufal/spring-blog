package com.blog.user.service;

import java.util.Optional;

import com.blog.user.model.User;

import java.util.List;

public interface IUserService {
  List<User> findAll();
  Optional<User> findOne(Long id);
  User create(User newUser);
  Optional<User> update(Long userId, User newData);
  Optional<User> delete(Long userId);
}