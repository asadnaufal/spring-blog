package com.blog.user.service;

import java.util.List;
import java.util.Optional;

import com.blog.user.model.User;
import com.blog.user.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements IUserService {

  @Autowired
  private UserRepository userRepository;

  @Override
  public List<User> findAll() {
    List<User> users = (List<User>) userRepository.findAll();

    return users;
  }

  @Override
  public Optional<User> findOne(Long id) {
    Optional<User> user = userRepository.findById(id);

    return user;
  }

  @Override
  public User create(User user) {
    return userRepository.save(user);
  }

  @Override
  public Optional<User> update(Long id, User newData) {
    Optional<User> user = userRepository.findById(id);

    if (user.isPresent()) {
      User unwrappeUser = user.get();
      unwrappeUser.setUsername(newData.getUsername());
      unwrappeUser.setEmail(newData.getEmail());
      unwrappeUser.setPassword(newData.getPassword());
      userRepository.save(unwrappeUser);
    }

    return user;
  }

  @Override
  public Optional<User> delete(Long id) {
    Optional<User> user = userRepository.findById(id);

    if (user.isPresent()) userRepository.deleteById(id);

    return user;
  }
}