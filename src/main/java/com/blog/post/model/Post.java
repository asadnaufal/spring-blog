package com.blog.post.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.GenerationType;

import com.blog.user.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="posts")
public class Post {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String title;

  private String content;

  @JoinColumn(name="user_id", referencedColumnName = "id", nullable = false)
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JsonIgnore
  private User user;

  // public Blog() {}

  // public Blog(String title, String content, User user) {
  //   this.title = title;
  //   this.content = content;
  //   this.user = user;
  // }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return this.content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public User getUser() {
    return this.user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public void update(Post post) {
    this.title = post.getTitle();
    this.content = post.getContent();
  }

}