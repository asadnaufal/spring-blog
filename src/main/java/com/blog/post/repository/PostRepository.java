package com.blog.post.repository;

import java.util.List;

import com.blog.post.model.Post;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
  List<Post> findByUserId(Long userId);
}