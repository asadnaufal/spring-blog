package com.blog.post.service;

import java.util.List;
import java.util.Optional;

import com.blog.post.model.Post;
import com.blog.post.repository.PostRepository;
import com.blog.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostService implements IPostService {

  @Autowired
  private PostRepository postRepository;

  @Autowired
  private UserRepository userRepository;

  @Override
  public List<Post> findAll() {
    List<Post> posts = (List<Post>) postRepository.findAll();
    return posts;
  }

  @Override
  public Optional<Post> findOne(Long postId) {
    return postRepository.findById(postId);
  }

  @Override
  public Post create(Long userId, Post postData) {
    return userRepository.findById(userId).map(user -> {
      postData.setUser(user);
      return postRepository.save(postData);
    }).orElse(postData);
  }

  @Override
  public Optional<Post> update(Long postId, Post postData) {
    return postRepository.findById(postId).map(oldPost -> {
      oldPost.update(postData);

      return postRepository.save(oldPost);
    });
  }

  @Override
  public Optional<Post> delete(Long postId) {
    return postRepository.findById(postId).map(post -> {
      postRepository.deleteById(postId);
      return post;
    });
  }

  @Override
  public List<Post> findUsersPosts(Long userId) {
    return postRepository.findByUserId(userId);
  }

}