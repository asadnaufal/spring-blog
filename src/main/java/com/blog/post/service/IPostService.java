package com.blog.post.service;

import java.util.List;
import java.util.Optional;

import com.blog.post.model.*;

public interface IPostService {
  List<Post> findAll();
  Optional<Post> findOne(Long postId);
  Post create(Long userId, Post postData);
  Optional<Post> update(Long postId, Post postData);
  Optional<Post> delete(Long postId);
  List<Post> findUsersPosts(Long userId);
}