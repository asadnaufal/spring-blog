package com.blog.post.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.blog.common.JSONResponse;
import com.blog.post.dtos.CreatePostDTO;
import com.blog.post.dtos.UpdatePostDTO;
import com.blog.post.model.Post;
import com.blog.post.service.IPostService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.modelmapper.ModelMapper;

@RestController
@RequestMapping(value = "/posts")
public class PostController {

  @Autowired
  private IPostService postsService;

  @Autowired
  private ModelMapper modelMapper;

  @GetMapping
  public JSONResponse<List<Post>> getPosts() {
    JSONResponse<List<Post>> response = new JSONResponse<>();
    response.setData(postsService.findAll());

    return response;
  }

  @GetMapping("/{postId}")
  public JSONResponse<Post> getPostById(@PathVariable("postId") Long postId) {
    JSONResponse<Post> response = new JSONResponse<>();

    Optional<Post> selectedPost = postsService.findOne(postId);

    if (selectedPost.isPresent()) {
      response.setData(selectedPost.get());
    } else {
      Map<String, Object> error = new HashMap<>();
      error.put("code", 404);
      error.put("message", String.format("post with id %d is not found", postId));
      response.setErrors(error);
    }

    return response;
  }

  @PostMapping
  public JSONResponse<Post> createPost(@RequestBody CreatePostDTO postData) {
    JSONResponse<Post> response = new JSONResponse<>();

    Post data = postsService.create(postData.getUserId(), convCreatePostDTOToEnity(postData));

    response.setData(data);

    return response;
  }

  @PutMapping(value = "/{postId}")
  public JSONResponse<Post> updatePost(@PathVariable("postId") Long postId, @RequestBody UpdatePostDTO postData) {
    System.out.println("postId" + postId);
    JSONResponse<Post> response = new JSONResponse<>();

    Optional<Post> updatedPost = postsService.update(postId, convPostDTOToEntity(postData));

    if (updatedPost.isPresent()) {
      response.setData(updatedPost.get());
    } else {
      Map<String, Object> error = new HashMap<>();
      error.put("code", 404);
      error.put("message", String.format("post with id %d is not found", postId));

      response.setErrors(error);
    }

    return response;
  }

  @DeleteMapping("/{postId}")
  public JSONResponse<Post> deletePostById(@PathVariable("postId") Long postId) {
    JSONResponse<Post> response = new JSONResponse<>();
    Optional<Post> deletedPost = postsService.delete(postId);

    if (deletedPost.isPresent()) {
      response.setData(deletedPost.get());
    } else {
      Map<String, Object> error = new HashMap<>();
      error.put("code", 404);
      error.put("message", String.format("post with id %d is not found", postId));

      response.setErrors(error);
    }

    return response;
  }

  public Post convCreatePostDTOToEnity(CreatePostDTO dto) {
    Post post = modelMapper.map(dto, Post.class);

    return post;
  }

  public <PostDTO> Post convPostDTOToEntity(PostDTO dto) {
    Post post = modelMapper.map(dto, Post.class);

    return post;
  }

}