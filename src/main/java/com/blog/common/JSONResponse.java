package com.blog.common;

public class JSONResponse<Data> {
  private Data data;
  private Object errors;

  public Data getData() {
    return this.data;
  }

  public void setData(Data data) {
    this.data = data;
  }

  public Object getErrors() {
    return this.errors;
  }

  public void setErrors(Object errors) {
    this.errors = errors;
  }

}